# MVCE

How to reproduce error logs?

* install java e.g. https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_windows-x64_bin.zip OR Java14 SDK
* create a GCP project as 'Standard App Engine java11': addlogic-foodiefnf-1
* create in same project a Cloud SQL instance 'as Postgres 13': quarkus
* set password for Cloud SQL in application.properties
* cmd: mvn clean package appengine:deploy

Call GCP address in Browser: /hello
-> errors appear in Log Explorer on GCP

See https://stackoverflow.com/questions/69350157/why-is-class-postgresql10dialect-not-found-on-quarkus-in-google-app-engine-java1


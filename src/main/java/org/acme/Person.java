package org.acme;

import javax.persistence.Entity;

import org.jboss.logging.Logger;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

/*
 * The natural person / user who log in. 
 */
@Entity
public class Person extends PanacheEntity {

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(Person.class);

	public String email;

	public String nick;

	public String password;
	

}